/*
 * Copyright (C) 2012,2013 Qianliang Zhang, Shawn Van Every, Samuel Audet
 *
 * IMPORTANT - Make sure the AndroidManifest.xml file looks like this:
 *
 * <?xml version="1.0" encoding="utf-8"?>
 * <manifest xmlns:android="http://schemas.android.com/apk/res/android"
 *     package="org.bytedeco.javacv.recordactivity"
 *     android:versionCode="1"
 *     android:versionName="1.0" >
 *     <uses-sdk android:minSdkVersion="4" />
 *     <uses-permission android:name="android.permission.CAMERA" />
 *     <uses-permission android:name="android.permission.INTERNET"/>
 *     <uses-permission android:name="android.permission.RECORD_AUDIO"/>
 *     <uses-permission android:name="android.permission.WAKE_LOCK"/>
 *     <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE"/>
 *     <uses-feature android:name="android.hardware.camera" />
 *     <application android:label="@string/app_name">
 *         <activity
 *             android:name="RecordActivity"
 *             android:label="@string/app_name"
 *             android:screenOrientation="landscape">
 *             <intent-filter>
 *                 <action android:name="android.intent.action.MAIN" />
 *                 <category android:name="android.intent.category.LAUNCHER" />
 *             </intent-filter>
 *         </activity>
 *     </application>
 * </manifest>
 *
 * And the res/layout/main.xml file like this:
 *
 * <LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
 *     xmlns:tools="http://schemas.android.com/tools"
 *     android:id="@+id/record_layout"
 *     android:layout_width="match_parent"
 *     android:layout_height="match_parent" >
 * 
 *     <TextView
 *         android:id="@+id/textView1"
 *         android:layout_width="wrap_content"
 *         android:layout_height="wrap_content"
 *         android:layout_centerHorizontal="true"
 *         android:layout_centerVertical="true"
 *         android:padding="8dp"
 *         android:text="@string/app_name"
 *         tools:context=".RecordActivity" />
 *
 *     <Button
 *         android:id="@+id/recorder_control"
 *         android:layout_width="wrap_content"
 *         android:layout_height="wrap_content"
 *         android:layout_above="@+id/textView1"
 *         android:layout_alignRight="@+id/textView1"
 *         android:layout_marginRight="70dp"
 *         android:text="Button" />
 *
 * </LinearLayout>
 */

package com.yuriy.javacv;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;

import org.bytedeco.javacpp.avcodec;
import org.bytedeco.javacpp.opencv_core;
import org.bytedeco.javacpp.opencv_highgui;
import org.bytedeco.javacv.FFmpegFrameRecorder;
import org.bytedeco.javacv.FrameRecorder;

public class MainActivity extends Activity {

    private static final String LOG_TAG = MainActivity.class.getSimpleName();

    private static final String VIDEO_FILE_PATH = "/mnt/sdcard/stream.mp4";
    private static final int FRAME_RATE = 30;
    private static final int DELAY = 10;

    private long mStartTime = 0;
    private boolean mDoRecording = false;

    private volatile FFmpegFrameRecorder mRecorder;

    private opencv_core.IplImage mImage;

    private final Handler handler = new Handler();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.main_activity_layout);

        initRecorder();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        mDoRecording = false;
    }

    private void initRecorder() {

        Log.i(LOG_TAG, "Init mRecorder");
        Log.i(LOG_TAG, "Video path : " + VIDEO_FILE_PATH);

        mImage = opencv_highgui.cvLoadImage("/sdcard/ford.jpg");
        Log.i(LOG_TAG, "Image " + mImage.width() + "x" + mImage.height());

        mRecorder = new FFmpegFrameRecorder(VIDEO_FILE_PATH, 320, 240);
        mRecorder.setFormat("mp4");
        mRecorder.setVideoCodec(avcodec.AV_CODEC_ID_H264);

        // Set in the surface changed method
        mRecorder.setFrameRate(FRAME_RATE);

        Log.i(LOG_TAG, "Recorder initialize successfully");
    }

    public void startRecording() {
        try {
            mRecorder.start();
        } catch (FrameRecorder.Exception e) {
            Log.e(LOG_TAG, "Start recording error:" + e.getMessage());
        }
        mStartTime = System.currentTimeMillis();
        mDoRecording = true;

        handler.postDelayed(runnable, DELAY);
    }

    private Runnable runnable = new Runnable() {

        @Override
        public void run() {
            /* do what you need to do */

            Log.v(LOG_TAG, "Writing Frame");
            final long time = 1000 * (System.currentTimeMillis() - mStartTime);
            if (time > mRecorder.getTimestamp()) {
                mRecorder.setTimestamp(time);
            }
            try {
                mRecorder.record(mImage);
            } catch (final FrameRecorder.Exception e) {
                Log.e(LOG_TAG, "Recording error:" + e.getMessage());
            }

            handler.postDelayed(this, DELAY);
        }
    };

    public void stopRecording() {
        if (mRecorder == null && !mDoRecording) {
            return;
        }

        mDoRecording = false;

        handler.removeCallbacks(runnable);

        Log.v(LOG_TAG,"Finishing recording, stop and release recorder");
        try {
            mRecorder.stop();
            mRecorder.release();
        } catch (FFmpegFrameRecorder.Exception e) {
            Log.e(LOG_TAG, "Stop recording error:" + e.getMessage());
        }
        mRecorder = null;
    }

    public void onStart(View v) {
        startRecording();
    }

    public void onStop(View v) {
        stopRecording();
    }
}
